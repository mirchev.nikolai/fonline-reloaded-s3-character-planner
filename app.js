const special = document.querySelector('.special-panel');
const skills = document.querySelector('.skills-panel');
const traits = document.querySelector('.traits-panel');
const infoPanel = document.querySelector('.info-panel');
const buildSpecial = document.querySelector('.special-panel-build');
const buildSkills = document.querySelector('.skills-panel-build');
const buildInfo1 = document.querySelector('.info-panels-build-1');
const buildInfo2 = document.querySelector('.info-panels-build-2');
const buildCommands = document.querySelector('.commands-panel-build');

const container = document.querySelector('.tab-content');
// SELECTORS Phase 2
const selectedSkill = document.querySelector('.skills-panel-build');

let currentLevel = 1;

let arrChar = [ // .[0] - Name
                [{}, 
                {},
                {}],

                // .[1] - SPECIAL
                [{id: 'str00', name: 'ST', value: 5, onDrugs: function() {return Number(arrChar[1][0].value + 2*arrChar[10][0].tagged + (-2*arrChar[10][5].tagged) + arrChar[10][2].tagged + (-2*arrChar[10][5].tagged))}, implanted: false},
                 {id: 'per01', name: 'PE', value: 5, onDrugs: function() {return Number(arrChar[1][1].value + arrChar[10][3].tagged + (-2*arrChar[10][2].tagged) + (-2*arrChar[10][4].tagged))}, implanted: false},
                 {id: 'end02', name: 'EN', value: 5, onDrugs: function() {return Number(arrChar[1][2].value + 2*arrChar[10][0].tagged + arrChar[10][2].tagged)}, implanted: false},
                 {id: 'cha03', name: 'CH', value: 5, onDrugs: function() {return Number(arrChar[1][3].value + 2*arrChar[10][8].tagged + arrChar[10][4].tagged + (-1*arrChar[10][3].tagged) + (-1*arrChar[10][5].tagged)) + 2*arrChar[10][7].tagged}, implanted: false},
                 {id: 'int04', name: 'IN', value: 5, onDrugs: function() {return Number(arrChar[1][4].value + 2*arrChar[10][7].tagged)}, implanted: false},
                 {id: 'agi05', name: 'AG', value: 5, onDrugs: function() {return Number(arrChar[1][5].value + (-1*arrChar[10][0].tagged) + arrChar[10][1].tagged)}, implanted: false},
                 {id: 'luc06', name: 'LK', value: 5, onDrugs: function() {return Number(arrChar[1][6].value)}, implanted: false},
                 {id: 'poo07', name: 'PL', value: 5}],

                // .[2] Skills
                [{id: 's-g00', tagged: false, max: 300, change: 0, spentSP: '', value: 0, func: function() {return 20*skillsArrChar[0].tagged + 4*SPECIALArrChar[5].value + 5;}, name: 'Small Guns'},
                 {id: 'b-g01', tagged: false, max: 300, change: 0, spentSP: '', value: 0, func: function() {return 20*skillsArrChar[1].tagged + 2*SPECIALArrChar[5].value;}, name: 'Big Guns'},
                 {id: 'e-w02', tagged: false, max: 300, change: 0, spentSP: '', value: 0, func: function() {return 20*skillsArrChar[2].tagged + 2*SPECIALArrChar[5].value;}, name: 'Energy Weapons'},
                 {id: 'c-c03', tagged: false, max: 300, change: 0, spentSP: '', value: 0, func: function() {return 20*skillsArrChar[3].tagged + 2*(SPECIALArrChar[0].value + SPECIALArrChar[5].value) + 30;}, name: 'Close Combat'},
                 {id: 'sca04', tagged: false, max: 100, change: 0, spentSP: '', value: 0, func: function() {return 0;}, name: 'Scavenging'},
                 {id: 'thr05', tagged: false, max: 300, change: 0, spentSP: '', value: 0, func: function() {return 20*skillsArrChar[5].tagged + 4*SPECIALArrChar[5].value;}, name: 'Throwing'},
                 {id: 'f-a06', tagged: false, max: 200, change: 0, spentSP: '', value: 0, func: function() {return 20*skillsArrChar[6].tagged + 2*(SPECIALArrChar[1].value + SPECIALArrChar[4].value);}, name: 'First Aid'},
                 {id: 'doc07', tagged: false, max: 200, change: 0, spentSP: '', value: 0, func: function() {return 20*skillsArrChar[7].tagged + SPECIALArrChar[1].value + SPECIALArrChar[4].value + 5;}, name: 'Doctor'},
                 {id: 'sne08', tagged: false, max: 270, change: 0, spentSP: '', value: 0, func: function() {return 20*skillsArrChar[8].tagged + 3*SPECIALArrChar[5].value + 5;}, name: 'Sneak'},
                 {id: 'loc09', tagged: false, max: 150, change: 0, spentSP: '', value: 0, func: function() {return 20*skillsArrChar[9].tagged + SPECIALArrChar[1].value + SPECIALArrChar[5].value + 10;}, name: 'Lockpick'},
                 {id: 'ste10', tagged: false, max: 150, change: 0, spentSP: '', value: 0, func: function() {return 20*skillsArrChar[10].tagged + 3*SPECIALArrChar[5].value;}, name: 'Steal'},
                 {id: 'tra11', tagged: false, max: 150, change: 0, spentSP: '', value: 0, func: function() {return 20*skillsArrChar[11].tagged + SPECIALArrChar[1].value + SPECIALArrChar[5].value + 10;}, name: 'Traps'},
                 {id: 'sci12', tagged: false, max: 125, change: 0, spentSP: '', value: 0, func: function() {return 20*skillsArrChar[12].tagged + 4*SPECIALArrChar[4].value;}, name: 'Science'},
                 {id: 'rep13', tagged: false, max: 125, change: 0, spentSP: '', value: 0, func: function() {return 20*skillsArrChar[13].tagged + 3*SPECIALArrChar[4].value;}, name: 'Repair'},
                 {id: 'spe14', tagged: false, max: 300, change: 0, spentSP: '', value: 0, func: function() {return 20*skillsArrChar[14].tagged + 5*SPECIALArrChar[3].value;}, name: 'Speech'},
                 {id: 'bar15', tagged: false, max: 150, change: 0, spentSP: '', value: 0, func: function() {return 20*skillsArrChar[15].tagged + 4*SPECIALArrChar[3].value;}, name: 'Barter'},
                 {id: 'gam16', tagged: false, max: 150, change: 0, spentSP: '', value: 0, func: function() {return 20*skillsArrChar[16].tagged + 5*SPECIALArrChar[6].value;}, name: 'Gambling'},
                 {id: 'out17', tagged: false, max: 175, change: 0, spentSP: '', value: 0, func: function() {return 20*skillsArrChar[17].tagged + 2*(SPECIALArrChar[2].value + SPECIALArrChar[4].value);}, name: 'Outdoorsman'}],

                // .[3] Traits
                [{id: 'f-m00', name: 'Fast Metabolism', tagged: false},
                 {id: 'bru01', name: 'Bruiser', tagged: false},
                 {id: 'o-h02', name: 'One Hander', tagged: false}, 
                 {id: 'fin03', name: 'Finesse', tagged: false},
                 {id: 'kam04', name: 'Kamikaze', tagged: false},
                 {id: 'hea05', name: 'Heavy Handed', tagged: false},
                 {id: 'f-s06', name: 'Fast Shot', tagged: false},
                 {id: 'b-m07', name: 'Bloody Mess', tagged: false},
                 {id: 'jin08', name: 'Jinxed', tagged: false},
                 {id: 'g-n09', name: 'Good Natured', tagged: false},
                 {id: 'c-r10', name: 'Chem Reliant', tagged: false},
                 {id: 'bon11', name: 'Bonehead', tagged: false},
                 {id: 'ski12', name: 'Skilled', tagged: false},
                 {id: 'lon13', name: 'Loner', tagged: false}],

                // .[4] Perks
                [{id: 'perk00', tagged: false, available: false, name: 'More Critical', func: function(){arrChar[4][0].available = (currentLevel >= 3 && arrChar[2][0].value >= 100 || arrChar[2][1].value >= 100 || arrChar[2][2].value >= 100 || arrChar[2][3].value >= 100 || arrChar[2][5].value >= 100);}},
                 {id: 'perk01', tagged: false, available: false, name: 'Quick Pockets', func: function(){arrChar[4][1].available = (currentLevel >= 3 && arrChar[1][0].value >= 5) ;}},
                 {id: 'perk02', tagged: false, available: false, name: 'Adrenaline Rush', func: function(){arrChar[4][2].available = (currentLevel >= 3 && arrChar[1][0].value >= 5) ;}},
                 {id: 'perk03', tagged: false, available: false, name: 'Quick Recovery', func: function(){arrChar[4][3].available = (currentLevel >= 3 && arrChar[1][5].value >= 6) ;}},
                 {id: 'perk04', tagged: false, available: false, name: 'Weapon Handling', func: function(){arrChar[4][4].available = (currentLevel >= 3 && arrChar[2][0].value >= 100 || arrChar[2][1].value >= 100 || arrChar[2][2].value >= 100 || arrChar[2][3].value >= 100 || arrChar[2][5].value >= 100) ;}},
                 {id: 'perk05', tagged: false, available: false, name: 'In Your Face!', func: function(){arrChar[4][5].available = (currentLevel >= 6 && arrChar[2][3].value >= 125) ;}},
                 {id: 'perk06', tagged: false, available: false, name: 'Even More Criticals', func: function(){arrChar[4][6].available = (currentLevel >= 6 && arrChar[2][0].value >= 125 || arrChar[2][1].value >= 125 || arrChar[2][2].value >= 125 || arrChar[2][3].value >= 125 || arrChar[2][5].value >= 125) ;}},
                 {id: 'perk07', tagged: false, available: false, name: 'Silent Running', func: function(){arrChar[4][7].available = (currentLevel >= 6 && arrChar[2][8].value >= 100) ;}},
                 {id: 'perk08', tagged: false, available: false, name: 'Toughness', func: function(){arrChar[4][8].available = (currentLevel >= 6 && arrChar[1][2].value >= 4) ;}},
                 {id: 'perk09', tagged: false, available: false, name: 'Sharpshooter', func: function(){arrChar[4][9].available = (currentLevel >= 9 && arrChar[1][4].value >= 3 && (arrChar[2][0].value >= 150 || arrChar[2][1].value >= 150 || arrChar[2][2].value >= 150 || arrChar[2][3].value >= 150 || arrChar[2][5].value >= 150)) ;}},
                 {id: 'perk10', tagged: false, available: false, name: 'Pyromaniac', func: function(){arrChar[4][10].available = (currentLevel >= 9 && arrChar[2][0].value >= 100 || arrChar[2][1].value >= 100 || arrChar[2][2].value >= 100 || arrChar[2][3].value >= 100 || arrChar[2][5].value >= 100) ;}},
                 {id: 'perk11', tagged: false, available: false, name: 'Close Combat Master', func: function(){arrChar[4][11].available = (currentLevel >= 9 && arrChar[2][3].value >= 150) ;}},
                 {id: 'perk12', tagged: false, available: false, name: 'Bonus Ranged Damage', func: function(){arrChar[4][12].available = (currentLevel >= 9 && arrChar[2][0].value >= 150 || arrChar[2][1].value >= 150 || arrChar[2][2].value >= 150 || arrChar[2][3].value >= 150 || arrChar[2][5].value >= 150) ;}},
                 {id: 'perk13', tagged: false, available: false, name: 'Even Tougher', func: function(){arrChar[4][13].available = (currentLevel >= 9 && arrChar[1][2].value >= 6) ;}},
                 {id: 'perk14', tagged: false, available: false, name: 'Stonewall', func: function(){arrChar[4][14].available = (currentLevel >= 9 && arrChar[1][0].value >= 6) ;}},
                 {id: 'perk15', tagged: false, available: false, name: 'Medic', func: function(){arrChar[4][15].available = (currentLevel >= 9 && arrChar[2][6].value >= 125 && arrChar[2][7].value >= 125 && arrChar[1][4].value >=3) ;}},
                 {id: 'perk16', tagged: false, available: false, name: 'Heave Ho!', func: function(){arrChar[4][16].available = (currentLevel >= 9 && arrChar[2][5].value >= 125) ;}},
                 {id: 'perk17', tagged: false, available: false, name: 'Gain Agility', func: function(){arrChar[4][17].available = (currentLevel >= 12 && arrChar[1][5].value < 10) ;}},
                 {id: 'perk18', tagged: false, available: false, name: 'Gain Charisma', func: function(){arrChar[4][18].available = (currentLevel >= 12 && arrChar[1][3].value < 10) ;}},
                 {id: 'perk19', tagged: false, available: false, name: 'Gain Endurance', func: function(){arrChar[4][19].available = (currentLevel >= 12 && arrChar[1][2].value < 10) ;}},
                 {id: 'perk20', tagged: false, available: false, name: 'Gain Intelligence', func: function(){arrChar[4][20].available = (currentLevel >= 12 && arrChar[1][4].value < 10) ;}},
                 {id: 'perk21', tagged: false, available: false, name: 'Gain Luck', func: function(){arrChar[4][21].available = (currentLevel >= 12 && arrChar[1][6].value < 10) ;}},
                 {id: 'perk22', tagged: false, available: false, name: 'Gain Perception', func: function(){arrChar[4][22].available = (currentLevel >= 12 && arrChar[1][1].value < 10) ;}},
                 {id: 'perk23', tagged: false, available: false, name: 'Gain Strength', func: function(){arrChar[4][23].available = (currentLevel >= 12 && arrChar[1][0].value < 10) ;}},
                 {id: 'perk24', tagged: false, available: false, name: 'Better Criticals', func: function(){arrChar[4][24].available = (currentLevel >= 12 && arrChar[2][0].value >= 175 || arrChar[2][1].value >= 175 || arrChar[2][2].value >= 175 || arrChar[2][3].value >= 175 || arrChar[2][5].value >= 175) ;}},
                 {id: 'perk25', tagged: false, available: false, name: 'Ghost', func: function(){arrChar[4][25].available = (currentLevel >= 12 && arrChar[2][8].value >= 150) ;}},
                 {id: 'perk26', tagged: false, available: false, name: 'Lifegiver', func: function(){arrChar[4][26].available = (currentLevel >= 12) ;}},
                 {id: 'perk27', tagged: false, available: false, name: 'Action Boy', func: function(){arrChar[4][27].available = (currentLevel >= 12 && arrChar[1][5].value >= 6) ;}},
                 {id: 'perk28', tagged: false, available: false, name: 'Action Boy', func: function(){arrChar[4][28].available = (currentLevel >= 15 && arrChar[1][5].value >= 6) ;}},
                 {id: 'perk29', tagged: false, available: false, name: 'Lifegiver', func: function(){arrChar[4][29].available = (currentLevel >= 15) ;}},
                 {id: 'perk30', tagged: false, available: false, name: 'Livewire', func: function(){arrChar[4][30].available = (currentLevel >= 15 && arrChar[1][5].value >= 6) ;}},
                 {id: 'perk31', tagged: false, available: false, name: 'Man of Steel', func: function(){arrChar[4][31].available = (currentLevel >= 15 && arrChar[1][2].value >= 8) ;}},
                 {id: 'perk32', tagged: false, available: false, name: 'Field Medic', func: function(){arrChar[4][32].available = (currentLevel >= 15 && arrChar[2][6].value >= 175 && arrChar[2][7].value >= 175 && arrChar[4][15].tagged) ;}},
                 {id: 'perk33', tagged: false, available: false, name: 'More Ranged Damage', func: function(){arrChar[4][33].available = (currentLevel >= 15 && arrChar[2][0].value >= 200 || arrChar[2][1].value >= 200 || arrChar[2][2].value >= 200 || arrChar[2][5].value >= 200) ;}},
                 {id: 'perk34', tagged: false, available: false, name: 'Silent Death', func: function(){arrChar[4][34].available = (currentLevel >= 15 && arrChar[2][8].value >= 175) ;}},
                 {id: 'perk35', tagged: false, available: false, name: 'Iron Limbs', func: function(){arrChar[4][35].available = (currentLevel >= 15 && arrChar[1][0].value >= 6 && arrChar[1][2].value >= 6) ;}},
                 {id: 'perk36', tagged: false, available: false, name: 'Dodger', func: function(){arrChar[4][36].available = (currentLevel >= 15 && arrChar[1][5].value >= 8 && arrChar[2][3].value >= 175) ;}},
                 {id: 'perk37', tagged: false, available: false, name: 'Dodger', func: function(){arrChar[4][37].available = (arrChar[1][5].value >= 10 && arrChar[2][3].value >= 175) ;}},
                 {id: 'perk38', tagged: false, available: false, name: 'Lifegiver', func: function(){arrChar[4][38].available = (currentLevel >= 18) ;}},
                 {id: 'perk39', tagged: false, available: false, name: 'Bonus Rate of Attack', func: function(){arrChar[4][39].available = (currentLevel >= 18 && arrChar[2][0].value >= 180 || arrChar[2][1].value >= 180 || arrChar[2][2].value >= 180 || arrChar[2][3].value >= 180 || arrChar[2][5].value >= 180) ;}}],
                 //
                 // .[5] Support Perks
                [{id: 'perk000', tagged: true, available: false, name: 'Awareness', func: function() {arrChar[5][0].available = (arrChar[1][0].value >= 0)}},
                 {id: 'perk001', tagged: false, available: false, name: 'Pack Rat', func: function() {arrChar[5][1].available = (currentLevel >= 6)}},
                 {id: 'perk002', tagged: false, available: false, name: 'Strong Back', func: function() {arrChar[5][2].available = (currentLevel >= 6 && arrChar[1][2].value >= 6)}},
                 {id: 'perk003', tagged: false, available: false, name: 'Boneyard Guard', func: function() {return}},
                 {id: 'perk004', tagged: false, available: false, name: 'Mr. Fixit', func: function() {arrChar[5][4].available = (arrChar[2][13].value >= 120)}},
                 {id: 'perk005', tagged: false, available: false, name: 'Dismantler', func: function() {arrChar[5][5].available = (arrChar[2][12].value >= 120)}},
                 {id: 'perk006', tagged: false, available: false, name: 'Dead Man Walking', func: function() {arrChar[5][6].available = (arrChar[1][4].value >= 5 && arrChar[2][7].value >= 50)}},
                 {id: 'perk007', tagged: false, available: false, name: 'Swift Learner', func: function() {arrChar[5][7].available = (arrChar[1][4].value >= 6 && arrChar[2][12].value >= 50)}},
                 {id: 'perk008', tagged: false, available: false, name: 'Faster Healing', func: function() {arrChar[5][8].available = (arrChar[1][4].value >= 6 && arrChar[2][7].value >= 75)}},
                 {id: 'perk009', tagged: false, available: false, name: 'Rad Resistance', func: function() {arrChar[5][9].available = (arrChar[1][4].value >= 7 && arrChar[2][7].value >= 100)}},
                 {id: 'perk010', tagged: false, available: false, name: 'Educated', func: function() {arrChar[5][10].available = (arrChar[1][4].value >= 8 && arrChar[2][12].value >= 100)}},
                 {id: 'perk011', tagged: false, available: false, name: 'Snakeater', func: function() {arrChar[5][11].available = (arrChar[1][2].value >= 6)}},
                 {id: 'perk012', tagged: false, available: false, name: 'Gecko Skinning', func: function() {arrChar[5][12].available = (arrChar[2][17].value >= 50)}},
                 {id: 'perk013', tagged: false, available: false, name: 'Cautious Nature', func: function() {arrChar[5][13].available = (arrChar[1][1].value >= 6 && arrChar[2][17].value >= 100)}},
                 {id: 'perk014', tagged: false, available: false, name: 'Ranger', func: function() {arrChar[5][14].available = (arrChar[2][17].value >= 100)}},
                 {id: 'perk015', tagged: false, available: false, name: 'Scout', func: function() {arrChar[5][15].available = (arrChar[2][17].value >= 150)}},
                 {id: 'perk016', tagged: false, available: false, name: 'Explorer', func: function() {arrChar[5][16].available = (arrChar[2][17].value >= 150)}},
                 {id: 'perk017', tagged: false, available: false, name: 'Pathfinder', func: function() {arrChar[5][17].available = (arrChar[2][17].value >= 150)}},
                 {id: 'perk018', tagged: false, available: false, name: 'Light Step', func: function() {arrChar[5][18].available = (arrChar[2][11].value >= 100)}},
                 {id: 'perk019', tagged: false, available: false, name: 'Demolition Expert', func: function() {arrChar[5][19].available = (arrChar[2][11].value >= 125)}},
                 {id: 'perk020', tagged: false, available: false, name: 'Negotiator', func: function() {arrChar[5][20].available = (arrChar[2][15].value >= 125)}},
                 {id: 'perk021', tagged: false, available: false, name: 'Sex Appeal', func: function() {arrChar[5][21].available = (arrChar[2][14].value >= 75)}},
                 {id: 'perk022', tagged: false, available: false, name: 'Magnetic Personality', func: function() {arrChar[5][22].available = (arrChar[2][14].value >= 100)}},
                 {id: 'perk023', tagged: false, available: false, name: 'Speaker', func: function() {arrChar[5][23].available = (arrChar[2][14].value >= 125)}},
                 {id: 'perk024', tagged: false, available: false, name: 'Stealth Girl', func: function() {arrChar[5][24].available = (arrChar[2][8].value >= 100 && arrChar[2][13].value >= 100)}},
                 {id: 'perk025', tagged: false, available: false, name: 'Thief', func: function() {arrChar[5][25].available = (arrChar[2][10].value >= 100)}},
                 {id: 'perk026', tagged: false, available: false, name: 'Harmless', func: function() {arrChar[5][26].available = (arrChar[1][3].value >= 6 && arrChar[2][10].value >= 125)}},
                 {id: 'perk027', tagged: false, available: false, name: 'Pickpocket', func: function() {arrChar[5][27].available = (arrChar[2][10].value >= 125)}},
                 {id: 'perk028', tagged: false, available: false, name: 'Master Thief', func: function() {arrChar[5][28].available = (arrChar[2][10].value >= 125)}},
                 {id: 'perk029', tagged: false, available: false, name: 'Treasure Hunter', func: function() {arrChar[5][29].available = (arrChar[2][9].value >= 125)}}],

                 // .[6] INFO
                [{id: 'i-h-p00', value: '0', func: function() {return 2*arrChar[1][2].value + arrChar[1][0].value + 55;}, name: 'Hit-Points'},
                 {id: 'i-a-c01', value: '0', func: function() {return 3*arrChar[1][5].value;}, name: 'Armor Class'},
                 {id: 'i-a-p02', value: '0', func: function() {return Math.floor((arrChar[1][5].value/2)) - 2*arrChar[3][1].tagged + 5;}, name: 'Action Points'},
                 {id: 'i-c-w03', value: '0', func: function() {return Math.floor(((arrChar[1][0].value*25 + 25)/2.2) + 20 - 0.5);}, name: 'Carry Weight'},
                 {id: 'i-m-d04', value: '0', func: function() {return !arrChar[3][1].tagged*(Math.floor(arrChar[1][0].value/7)*(arrChar[1][0].value - 6) + 1) + 2*arrChar[3][1].tagged*(Math.floor(arrChar[1][0].value/7)*((arrChar[1][0].value - 6) + 1)) + arrChar[3][1].tagged*Math.floor((arrChar[1][0].value%7)/arrChar[1][0].value);}, name: 'Melee Damage'},
                 {id: 'i-d-r05', value: '0', func: function() {return -10*arrChar[3][4].tagged + 0;}, name: 'Damage Resistance'},
                 {id: 'i-p-r06', value: '0', func: function() {return 3*arrChar[1][2].value;}, name: 'Poison Resistance'},
                 {id: 'i-r-r07', value: '0', func: function() {return 2*arrChar[1][2].value;}, name: 'Rad Resistance'},
                 {id: 'i-seq08', value: '0', func: function() {return 2*arrChar[1][1].value + 2;}, name: 'Sequence'},
                 {id: 'i-h-r09', value: '0', func: function() {return Math.floor(arrChar[1][2].value/2) + 7;}, name: 'Healing Rate'},
                 {id: 'i-c-c10', value: '0', func: function() {return arrChar[1][6].value + 10*arrChar[3][3].tagged;}, name: 'Critical Chance'},
                 {id: 'i-p-p11', value: '0', func: function() {return 10*arrChar[1][3].value + 50*arrChar[3][9].tagged + Math.floor((arrChar[2][14].value)/3);}, name: 'Party Points'},
                 {id: 'i-m-p12', value: '0', func: function() {return 8 - 2*arrChar[3][12].tagged;}, name: 'Maximum Perks'},
                 {id: 'i-spl13', value: '0', func: function() {return 2*arrChar[1][4].value + 5*arrChar[3][12].tagged + 2*arrChar[5][10].tagged + 5;}, name: 'Skill Points/lvl'},
                 {id: 'i-hpl14', value: '0', func: function() {if (arrChar[1][2].value%2) return Math.floor(arrChar[1][2].value/2) + 2 + '/2lvl'; if (!(arrChar[1][2].value%2)) return (arrChar[1][2].value)/2;}, name: 'Hit-Points/lvl'},
                 {id: 'i-d-t15', value: '0', func: function() {return;}, name: 'Hit-Points/lvl'}],
                 
                 // .[7] Books
                [{id: 'S-G00-book', read: false, name: 'S.Guns'},
                 {id: 'F-A06-book', read: false, name: 'F.Aid'},
                 {id: 'OUT17-book', read: false, name: 'OD'},
                 {id: 'REP13-book', read: false, name: 'Repair'},
                 {id: 'SCI12-book', read: false, name: 'Science'},
                 {id: 'BAR15-book', read: false, name: 'Barter'}
                ],
                 
                 // .[8] Implants
                [{id: 'str00-implanted', tagged: false, available: false, func: function(){arrChar[8][0].available = (currentLevel >= 30 && arrChar[1][0].value < 10)}},
                 {id: 'per01-implanted', tagged: false, available: false, func: function(){arrChar[8][1].available = (currentLevel >= 30 && arrChar[1][1].value < 10)}},
                 {id: 'end02-implanted', tagged: false, available: false, func: function(){arrChar[8][2].available = (currentLevel >= 30 && arrChar[1][2].value < 10)}},
                 {id: 'cha03-implanted', tagged: false, available: false, func: function(){arrChar[8][3].available = (currentLevel >= 30 && arrChar[1][3].value < 10)}},
                 {id: 'int04-implanted', tagged: false, available: false, func: function(){arrChar[8][4].available = (currentLevel >= 30 && arrChar[1][4].value < 10)}},
                 {id: 'agi05-implanted', tagged: false, available: false, func: function(){arrChar[8][5].available = (currentLevel >= 30 && arrChar[1][5].value < 10)}},
                 {id: 'luc06-implanted', tagged: false, available: false, func: function(){arrChar[8][6].available = (currentLevel >= 30 && arrChar[1][6].value < 10)}},
                 {id: 'der07-implanted', tagged: false, available: false, func: function(){arrChar[8][7].available = (currentLevel >= 30)}},
                 {id: 'pho08-implanted', tagged: false, available: false, func: function(){arrChar[8][8].available = (currentLevel >= 30)}},
                 {id: 'nem09-implanted', tagged: false, available: false, func: function(){arrChar[8][9].available = (currentLevel >= 30)}},
                 {id: 'der10-implanted', tagged: false, available: false, func: function(){arrChar[8][10].available = (currentLevel >= 30 && arrChar[8][7].tagged)}},
                 {id: 'pho11-implanted', tagged: false, available: false, func: function(){arrChar[8][11].available = (currentLevel >= 30 && arrChar[8][8].tagged)}},
                 {id: 'nem12-implanted', tagged: false, available: false, func: function(){arrChar[8][12].available = (currentLevel >= 30 && arrChar[8][9].tagged)}}],
                 // .[9] Proffesions
                [{id: 'sg-prof', tagged: false, available: false, name: 'S.Guns', func: function () {arrChar[9][0].available = (arrChar[2][13].value >= 50)}},
                 {id: 'sg-prof', tagged: false, available: false, name: 'S.Guns', func: function () {arrChar[9][1].available = (arrChar[9][0].tagged && arrChar[2][13].value >= 100)}},
                 {id: 'bg-prof', tagged: false, available: false, name: 'Big Guns', func: function () {arrChar[9][2].available = (arrChar[2][13].value >= 50)}},
                 {id: 'bg-prof', tagged: false, available: false, name: 'Big Guns', func: function () {arrChar[9][3].available = (arrChar[9][2].tagged && arrChar[2][13].value >= 100)}},
                 {id: 'ew-prof', tagged: false, available: false, name: 'Energy', func: function () {arrChar[9][4].available = (arrChar[2][12].value >= 50)}},
                 {id: 'ew-prof', tagged: false, available: false, name: 'Energy', func: function () {arrChar[9][5].available = (arrChar[9][4].tagged && arrChar[2][12].value >= 100)}},
                 {id: 'ar-prof', tagged: false, available: false, name: 'Armorer', func: function () {arrChar[9][6].available = (arrChar[2][13].value >= 50)}},
                 {id: 'ar-prof', tagged: false, available: false, name: 'Armorer', func: function () {arrChar[9][7].available = (arrChar[9][6].tagged && arrChar[2][13].value >= 100)}},
                 {id: 'de-prof', tagged: false, available: false, name: 'D.Expert', func: function () {arrChar[9][8].available = (arrChar[2][11].value >= 50)}},
                 {id: 'de-prof', tagged: false, available: false, name: 'D.Expert', func: function () {arrChar[9][9].available = (arrChar[9][8].tagged && arrChar[2][11].value >= 100)}},
                 {id: 'dc-prof', tagged: false, available: false, name: 'Doctor', func: function () {arrChar[9][10].available = (arrChar[2][7].value >= 50)}},
                 {id: 'dc-prof', tagged: false, available: false, name: 'Doctor', func: function () {arrChar[9][11].available = (arrChar[9][10].tagged && arrChar[2][7].value >= 100)}}
                ],

                // .[10] Drugs
                [{id: 'drug0', tagged: false, name: 'Buffout'},
                 {id: 'drug1', tagged: false, name: 'Nuka Cola'},
                 {id: 'drug2', tagged: false, name: 'Psycho'},
                 {id: 'drug3', tagged: false, name: 'Cigarettes'},
                 {id: 'drug4', tagged: false, name: 'Beer'},
                 {id: 'drug5', tagged: false, name: 'Jet'},
                 {id: 'drug6', tagged: false, name: 'Rad-X'},
                 {id: 'drug7', tagged: false, name: 'Mentats'},
                 {id: 'drug8', tagged: false, name: `Charisma Boost +2\nArmor +1 CH\nMirrored Shades +1 CH`}
                ],

                // .[11] Marshal Boosts
                [{id: 's-g00-boost', tagged: false, available: false, func: function() {return (arrChar[2][0].value < 66)}, name: 'SG +10%'},
                 {id: 'b-g01-boost', tagged: false, available: false, func: function() {return (arrChar[2][1].value < 66)}, name: 'BG +10%'},
                 {id: 'e-w02-boost', tagged: false, available: false, func: function() {return (arrChar[2][2].value < 66)}, name: 'EW +10%'},
                 {id: 'c-c03-boost', tagged: false, available: false, func: function() {return (arrChar[2][3].value < 91)}, name: 'CC +10%'},
                 {id: false},
                 {id: 'thr05-boost', tagged: false, available: false, func: function() {return (arrChar[2][5].value < 66)}, name: 'TH +10%'}]
];        

let nameArrChar = arrChar[0];
let SPECIALArrChar = arrChar[1];
let skillsArrChar = arrChar[2]; 
let traitsArrChar = arrChar[3]; 
let perksArrChar = arrChar[4];
let supportPerksArrChar = arrChar[5];
let taggedSkills = 0;
let taggedTraits = 0;


let pool = SPECIALArrChar[7].value;


let availablePerk = 0;
let nextPerkAtLevel = 0;
const withoutSkilled = [3, 6, 9, 12, 15, 18, 21, 24];
const withSkilled = [4, 8, 12, 16, 20, 24, 30, 30];
let requiredLevel = [];
let unspentSP = 0;
let currentSkillId = '';
let stepArrChar = [];
let stepSkillsArrChar = [];
let stepSPECIALArrChar = [];

refresh();

// LISTENERS Phase 2
selectedSkill.addEventListener('click', function(){
    displaySkill();
});

container.addEventListener('click', function(e){
    console.log(e.target);
});


function modifySPECIAL (value) {
    let modifier = value.slice(-1);
    let statIndex = Number(value.slice(3, -1));
    let statId = value.slice(0, -1);
    let statValue = Number(SPECIALArrChar[statIndex].value);
    if (modifier === '+') {
        if (pool > 0 && statValue < 10) {
            statValue += 1;
            pool -= 1;
        } else if (pool === 0) {
            return;
        }
    } else {
        if (statValue > 1) {
            statValue -= 1;
            pool += 1;
        }
    }

    SPECIALArrChar[statIndex].value = statValue;
    SPECIALArrChar[7].value = pool;
    document.getElementById(`${statId}`).value = statValue;
    document.getElementById('poo07').value = pool;

    // check for Bruiser
    if ((SPECIALArrChar[0].value + pool) < 5 && traitsArrChar[1].tagged) {
        document.getElementById('bru01').disabled = true;
    } else {
        document.getElementById('bru01').disabled = false;
    }
    
    // check for Bonehead
    if (SPECIALArrChar[4].value + pool < 2) {
        document.getElementById('bon11').disabled = true;
    } else {
        document.getElementById('bon11').disabled = false;
    }


    refresh();
}

function tagSkill() {
    // console.clear();
    // Checking for number of tagged skills and disabling/enabling the rest
    const skills = document.querySelectorAll('input[name="skills"]');
    let counter = 0; // counts tagged skills
    let i = 0; // counts the outer loop
    for (const skill of skills) {    // first iteration over all skills
        if (skill.checked) {
            counter++;
            skillsArrChar[i].tagged = true;  // updates arrChar 
            document.getElementById(`${skill.id}`).parentElement.parentElement.classList.add('tagged-skill');
        } else {
            skillsArrChar[i].tagged = false;
            document.getElementById(`${skill.id}`).parentElement.parentElement.classList.remove('tagged-skill');
        }

        if (counter >= 3) { // if 3 skills are tagged
            for (const skill of skills) {  // second iteration over all skills
                if (!skill.checked) {
                    document.getElementById(`chkbx-${skill.value}`).disabled = true; // disables all skills except the 3 tagged
                }
            }
        } else {
            document.getElementById(`chkbx-${skill.value}`).disabled = false; // keeps all skills enabled
            document.getElementById('chkbx-sca04').disabled = true; // keeps Scavenging disabled at all times
        }
        skillsArrChar[i].value = skillsArrChar[i].func(); // calculates and stores Skill value in arrChar for every skill
        let docId = skillsArrChar[i].id;
        document.getElementById(`${docId}`).value = skillsArrChar[i].value + '%';  //displays current skill value in the panel
        // console.log(skillsArrChar[i].name + ' ' + skillsArrChar[i].value + '  tagged: ' + skillsArrChar[i].tagged);
        taggedSkills = counter;
        i++;
    }
    refresh();
  }

function tagTrait() {
  console.clear();
    // Checking for number of tagged traits and disabling/enabling the rest
    const traits = document.querySelectorAll('input[name="traits"]');
    let counter = 0; // counts tagged traits
    let i = 0; // counts the outer loop
    for (const trait of traits) {    // first iteration over all traits
        if (trait.checked) {
            counter++;
            traitsArrChar[i].tagged = true;  // updates arrChar 
        } else {
            traitsArrChar[i].tagged = false;
        }

        if (counter >= 2) { // if 3 traits are tagged
            for (const trait of traits) {  // second iteration over all traits
                if (!trait.checked) {
                    document.getElementById(`${trait.id}`).disabled = true; // disables all traits except the 2 tagged
                }
            }
        } else {
            document.getElementById(`${trait.id}`).disabled = false; // keeps all traits enabled

        }
        
        console.log(traitsArrChar[i].name + ' ' + '  tagged: ' + traitsArrChar[i].tagged);
        taggedTraits = counter;
        i++;
    }
    refresh();
}

function tagBruiserOrBH (id) {
    let ST = SPECIALArrChar[0].value;
    let IN = SPECIALArrChar[4].value;
    if (id === 'bru01' && !traitsArrChar[1].tagged) {
        console.log('slagam bruiser');
        if (ST < 7) {
            ST += 4;
        } else if (ST < 10) {
            let change = 4 - (10 - ST);
            ST = 10;
            pool += change;
        } else {
            pool += 4;
        }
    } else if (id === 'bru01' && traitsArrChar[1].tagged) {
        console.log('maham bruiser');
        if (ST > 4) {
            ST -= 4;
        } else if (ST < 5) {
            let change = 5 - ST;
            ST = 1;
            pool -= change;
        }
    }
    SPECIALArrChar[0].value = ST;
    
    if (id === 'bon11' && !traitsArrChar[11].tagged) {
        if (IN > 1) {
            IN -= 1;
        } else  {
            pool -= 1;
        }
    } else if (id === 'bon11' && traitsArrChar[11].tagged) {
        if (IN < 10) {
            IN += 1;
        } else {
            pool += 1;
        }
    }
    SPECIALArrChar[4].value = IN;
    SPECIALArrChar[7].value = pool;
    refresh();
    tagTrait();
}

function refresh () {
    document.getElementById('skills-counter').value = 3 - taggedSkills;
    document.getElementById('traits-counter').value = 2 - taggedTraits;

    let i = 0;
    // Updates SPECIAL stats
    arrChar[1].forEach (stat => {
        document.getElementById(`${stat.id}`).value = SPECIALArrChar[i].value;
        i++;
    });

    // Updates Skills
    i = 0;
    arrChar[2].forEach (skill => {
        skillsArrChar[i].value = skillsArrChar[i].func();
        document.getElementById(`${skill.id}`).value = skillsArrChar[i].value + '%';
        i++;
    });

    // Updates Info panel
    i = 0;
    arrChar[6].forEach (info => {
        arrChar[6][i].value = arrChar[6][i].func();
        document.getElementById(`${info.id}`).value = arrChar[6][i].value;
        i++;
    });
}

function printOutput() {
    // arrChar[4][26].func();
    // console.log(arrChar[4][26].available);
    // console.log(SPECIALArrChar);
    // console.log(arrChar[5]);
    console.log(arrChar[9]);
}

function submitChar () {
    // CHECKS for skills and SPECIAL

    // Data Move
    stepSkillsArrChar = JSON.parse(JSON.stringify(skillsArrChar));
    stepSPECIALArrChar = JSON.parse(JSON.stringify(SPECIALArrChar));
    let i = 0;
    let checked = false;
    stepSkillsArrChar.forEach (skill => {
        document.getElementById(`${(skill.id).toUpperCase()}`).value = stepSkillsArrChar[i].value + '%';
        if (skill.tagged) document.getElementById(`${(skill.id).toUpperCase()}`).parentElement.parentElement.classList.add('tagged-skill');
        if (!checked && skill.tagged) {
            document.getElementById(`r-${(skill.id).toUpperCase()}`).checked = true;
            checked++;
            displaySkill();
        }
        i++;
    });
    i = 0;
    stepSPECIALArrChar.forEach (stat => {
        if (i > 6) return;
        document.getElementById(`${(stat.id).toUpperCase()}`).value = stepSPECIALArrChar[i].value;
        i++;
    });

    i = 0;
    arrChar[1].forEach(stat => {
        if (i > 6) return;
        const drugId = `drugs-${(stat.id).toUpperCase()}`;
        const specialValue = arrChar[1][i].onDrugs();
        if (specialValue > 10) {
            document.getElementById(drugId).value = 10;
        } else if (specialValue < 1) {
            document.getElementById(drugId).value = 1;
        } else {
            document.getElementById(drugId).value = specialValue;
        }
        i++;
    }); 

    special.classList.add('hidden');
    skills.classList.add('hidden');
    traits.classList.add('hidden');
    infoPanel.classList.add('hidden');
    buildSpecial.classList.remove('hidden');
    buildSkills.classList.remove('hidden');
    buildInfo1.classList.remove('hidden');
    buildInfo2.classList.remove('hidden');
    buildCommands.classList.remove('hidden');

    updateProgressBars();
    availableProfessions();
    availablePerks();
    availableSupportPerks();
    availableBoosts();
    availableImplants();

    document.getElementById('x-level5').value = requiredLevel[0];
    document.getElementById('x-level5-span').textContent = `x${requiredLevel[0]}`;

}

// PHASE 2

function displaySkill () {
    
        const skills = document.querySelectorAll('input[name="radio-skill"]');
                for (const skill of skills) {
                    
                    if (skill.checked) {
                        let id = Number((skill.id).slice(-2));
                        let max = stepSkillsArrChar[id].max;
                        let skillValue = skillsArrChar[id].value;
                        document.getElementById('display-skill').value = stepSkillsArrChar[id].name;
                        document.getElementById('display-skill-value').value = `${skillValue}%`;
                        document.getElementById(skill.id).parentElement.parentElement.classList.add('selected-skill');
                        currentSkillId = (skill.id).slice(-5);
                        document.getElementById('skill-progress-bar').style = `width: ${skillValue/(max/100)}%`;
                        document.getElementById('skill-progress-bar').textContent = `${Math.round(skillValue/(max/100))}%`;
                    } else {
                        document.getElementById(skill.id).parentElement.parentElement.classList.remove('selected-skill');
                    }

                }
        
    
}

function moveSP (value) {
    let skillModifier = Number(document.querySelector('input[name="x-skill"]:checked').value);
    for (let i = 0; i < skillModifier; i++) {
        modifySkill(value);
        updateProgressBars();
        document.getElementById('unspent-sp').value = unspentSP;

    }
    
}

function modifySkill (value) {
    let skillIndex = Number(currentSkillId.slice(-2));
    let tempValue = Number(skillsArrChar[skillIndex].value);
    let stepValue = Number(stepSkillsArrChar[skillIndex].value);
    let max = skillsArrChar[skillIndex].max;
    let string = skillsArrChar[skillIndex].spentSP;
    
    if (value === '+') {
            if (unspentSP > 0 && tempValue < max && !skillsArrChar[skillIndex].tagged) {                
                    if (tempValue < 101) {
                            tempValue++;
                            unspentSP--; 
                            skillsArrChar[skillIndex].spentSP += '1';
                    } else if (tempValue > 100 && tempValue < 126 && unspentSP > 1) {
                            tempValue++;
                            unspentSP -= 2; 
                            skillsArrChar[skillIndex].spentSP += '2';
                    } else if (tempValue > 125 && tempValue < 151 && unspentSP > 2) {
                            tempValue++;
                            unspentSP -= 3; 
                            skillsArrChar[skillIndex].spentSP += '3';
                    } else if (tempValue > 150 && tempValue < 176 && unspentSP > 3) {
                            tempValue++;
                            unspentSP -= 4; 
                            skillsArrChar[skillIndex].spentSP += '4';
                    } else if (tempValue > 175 && tempValue < 201 && unspentSP > 4) {
                            tempValue++;
                            unspentSP -= 5; 
                            skillsArrChar[skillIndex].spentSP += '5';
                    } else if (tempValue > 200 && tempValue < 300 && unspentSP > 5) {
                            tempValue++;
                            unspentSP -= 6; 
                            skillsArrChar[skillIndex].spentSP += '6';
                    }

                    // skillsArrChar[skillIndex].value = tempValue;
                    // document.getElementById(currentSkillId).value = `${tempValue}%`;
                    // document.getElementById('display-skill-value').value = `${tempValue}%`;
                    // document.getElementById('skill-progress-bar').style = `width: ${Math.round(tempValue/(max/100))}%`;
                    // document.getElementById('skill-progress-bar').textContent = `${Math.round(tempValue/(max/100))}%`;
                    
                    // console.clear();
                    // console.log('Skill MAX: ' + max);
                    // console.log('Current Value: ' + tempValue);
                    // console.log('Unspent SP: ' + unspentSP);   
                    // console.log(skillsArrChar[skillIndex].spentSP);        
            } else if (unspentSP > 0 && tempValue < max && skillsArrChar[skillIndex].tagged) {
                
                if (tempValue < 101) {
                            tempValue += 2;
                            unspentSP--;
                            skillsArrChar[skillIndex].spentSP += '1';
                    } else if (tempValue > 100 && tempValue < 126 && unspentSP > 1) {
                            tempValue += 2;
                            unspentSP -= 2;
                            skillsArrChar[skillIndex].spentSP += '2';
                    } else if (tempValue > 125 && tempValue < 151 && unspentSP > 2) {
                            tempValue += 2;
                            unspentSP -= 3; 
                            skillsArrChar[skillIndex].spentSP += '3';
                    } else if (tempValue > 150 && tempValue < 176 && unspentSP > 3) {
                            tempValue += 2;
                            unspentSP -= 4; 
                            skillsArrChar[skillIndex].spentSP += '4';
                    } else if (tempValue > 175 && tempValue < 201 && unspentSP > 4) {
                            tempValue += 2;
                            unspentSP -= 5; 
                            skillsArrChar[skillIndex].spentSP += '5';
                    } else if (tempValue > 200 && tempValue < 300 && unspentSP > 5) {
                            tempValue += 2;
                            unspentSP -= 6; 
                            skillsArrChar[skillIndex].spentSP += '6';
                    }

                skillsArrChar[skillIndex].value = tempValue;

                if (tempValue > max) {
                    skillsArrChar[skillIndex].change = tempValue - max;
                    tempValue = max;
                    skillsArrChar[skillIndex].value = max;
                    document.getElementById(currentSkillId).value = `${max}%`;
                    document.getElementById('display-skill-value').value = `${max}%`;
                    document.getElementById('skill-progress-bar').style = `width: ${Math.round(tempValue/(max/100))}%`;
                    document.getElementById('skill-progress-bar').textContent = `${Math.round(tempValue/(max/100))}%`;
                } else {
                    document.getElementById(currentSkillId).value = `${tempValue}%`;
                    document.getElementById('display-skill-value').value = `${tempValue}%`;
                    document.getElementById('skill-progress-bar').style = `width: ${Math.round(tempValue/(max/100))}%`
                    document.getElementById('skill-progress-bar').textContent = `${Math.round(tempValue/(max/100))}%`;
                }
                // console.clear();
                // console.log('Skill MAX: ' + max);
                // console.log('Current Value: ' + tempValue);
                // console.log('Unspent SP: ' + unspentSP);
                // console.log(skillsArrChar[skillIndex].change);
            } else {
                return;
            }
} else if (value === '-') {

            if (tempValue > stepValue && !skillsArrChar[skillIndex].tagged) {
                if (tempValue < 102) {
                        tempValue--;
                        unspentSP++;
                } else if (tempValue > 101 && tempValue < 127) {
                        tempValue--;
                        unspentSP += 2; 
                } else if (tempValue > 126 && tempValue < 152) {
                        tempValue--;
                        unspentSP += 3; 
                } else if (tempValue > 151 && tempValue < 177) {
                        tempValue--;
                        unspentSP += 4; 
                } else if (tempValue > 176 && tempValue < 202) {
                        tempValue--;
                        unspentSP += 5; 
                } else if (tempValue > 201 && tempValue < 302) {
                        tempValue--;
                        unspentSP += 6;
                }
                // skillsArrChar[skillIndex].value = tempValue;

                // skillsArrChar[skillIndex].spentSP = string.slice(0, -1);
                // document.getElementById(currentSkillId).value = `${tempValue}%`;
                // document.getElementById('display-skill-value').value = `${tempValue}%`;
                // document.getElementById('skill-progress-bar').style = `width: ${tempValue/(max/100)}%`
                // document.getElementById('skill-progress-bar').textContent = `${Math.round(tempValue/(max/100))}%`;


                // console.clear();
                // console.log('Skill MAX: ' + max);
                // console.log('Current Value: ' + tempValue);
                // console.log('Unspent SP: ' + unspentSP);
                // console.log(skillsArrChar[skillIndex].spentSP);

            } else if (tempValue > stepValue && skillsArrChar[skillIndex].tagged) {
                
                let num = Number(string.slice(-1));
                unspentSP += num;
                skillsArrChar[skillIndex].spentSP = string.slice(0, -1);
                if (skillsArrChar[skillIndex].change > 0 && tempValue === max) {
                    tempValue -= 1;
                } else {
                    tempValue -= 2;
                }
                // skillsArrChar[skillIndex].value = tempValue;


                // document.getElementById(currentSkillId).value = `${tempValue}%`;
                // document.getElementById('display-skill-value').value = `${tempValue}%`;
                // document.getElementById('skill-progress-bar').style = `width: ${tempValue/(max/100)}%`;
                // document.getElementById('skill-progress-bar').textContent = `${Math.round(tempValue/(max/100))}%`;

                // console.clear();
                // console.log('Skill MAX: ' + max);
                // console.log('Current Value: ' + tempValue);
                // console.log('Unspent SP: ' + unspentSP);
                // console.log(skillsArrChar[skillIndex].spentSP);
            }
                
    }
    skillsArrChar[skillIndex].value = tempValue;

    document.getElementById(currentSkillId).value = `${tempValue}%`;
    document.getElementById('display-skill-value').value = `${tempValue}%`;
    document.getElementById('skill-progress-bar').style = `width: ${tempValue/(max/100)}%`;
    document.getElementById('skill-progress-bar').textContent = `${Math.round(tempValue/(max/100))}%`;

    availablePerks();
    availableSupportPerks();
    availableProfessions();
    availableBoosts();
}

function levelUp () {
    // CHECKS

    // STEP Actions
    let SP = arrChar[6][13].func();
    unspentSP += SP;
    currentLevel += 1;
    
    document.getElementById('unspent-sp').value = unspentSP;
    document.getElementById('lvl').value = currentLevel;

    stepSkillsArrChar = JSON.parse(JSON.stringify(skillsArrChar));
    if (currentLevel === 30){
        availablePerk = 0;
        document.getElementById('perks-tab-nav').classList.add('hidden');
        document.getElementById('perks-tab-content').classList.add('hidden');
        document.getElementById('perks-tab').classList.add('hidden-2');
        document.getElementById('perks-tab').disabled = true;

        document.getElementById('x-level10').disabled = false;
        document.getElementById('x-level10').title = '';
        document.getElementById('x-level100').disabled = false;
        document.getElementById('x-level100').title = '';
        document.getElementById('x-level5').value = 5;
        document.getElementById('x-level5-span').innerText = 'x5';

        $('.nav-tabs a[href="#support-perks-tab"]').tab('show');
    }

    if (currentLevel < 30) availablePerks();
    if (currentLevel >= 30) availableImplants();

    availableSupportPerks();
    availableProfessions();
    availableBoosts();    
}

function moveLevel () {
    let levelModifier = Number(document.querySelector('input[name="x-level"]:checked').value);
    
    for (let i = 0; i < levelModifier; i++) {
        levelUp();
        document.getElementById('unspent-sp').value = unspentSP;
    }
    
}

function readBooks (value) {
    let bookIndex = value.slice(0,1);
    let singleBooksRead = 0;
    arrChar[7][bookIndex].read = true;
    value = value.slice(-5);   
    document.getElementById(`r-${value}`).checked = true;
    displaySkill();

    let cacheSP = unspentSP;
        for (let i = 0; i < 10; i++) {
            unspentSP = 6;
            let index = Number(currentSkillId.slice(-2));
            let max = arrChar[2][index].max;
            let currentSkillValue = arrChar[2][index].value;

            if (currentSkillValue < max) {
                for ( let j = 0; j < 6; j++) {
                        modifySkill('+');
                    }
                singleBooksRead++;
            } else if (currentSkillValue >= max) {
                singleBooksRead = i;
                i = 10;
            }
          
        }

    unspentSP = cacheSP;
    const btnId = value + '-book';
    document.getElementById(`${btnId}`).classList.add('hidden');
    const displayId = value + '-display';
    document.getElementById(`${displayId}`).classList.remove('hidden');
    document.getElementById(`${displayId}`).innerText = `${arrChar[7][bookIndex].name}(${singleBooksRead})`;
    let booksRead = 0;
    arrChar[7].forEach(book => {
        if (book.read) booksRead++;
        if (booksRead === 6) {
            document.getElementById('available-books-header').classList.add('hidden');
            document.getElementById('books-panel').classList.add('hidden');
        }
    });
    console.log(singleBooksRead);
    stepSkillsArrChar = JSON.parse(JSON.stringify(skillsArrChar));
    availableSupportPerks();
    updateProgressBars();
}

function availablePerks () {

arrChar[4].forEach( perk => {
perk.func();
const perkID = (perk.id).slice(-2) + 'perk';
if(perk.available) {
    document.getElementById(`${perkID}`).parentElement.classList.remove('hidden');
} else {
    document.getElementById(`${perkID}`).parentElement.classList.add('hidden');
}
});

let modul = 0;
if (arrChar[3][12].tagged) {
    requiredLevel = withSkilled.slice();
} else {
    requiredLevel = withoutSkilled.slice();
}
modul = currentLevel%requiredLevel[0];

if (currentLevel === 2) {
    nextPerkAtLevel = requiredLevel[0];
    }

if (currentLevel === nextPerkAtLevel) {
    document.getElementById('x-level5').disabled = false;
    document.getElementById('x-level5').title = '';
}

if (!modul && currentLevel <= 24 && currentLevel >= nextPerkAtLevel) {
    availablePerk = 1;
    $('.nav-tabs a:first').tab('show');
}

if (!availablePerk) {
    document.getElementById('perks-tab-link').classList.add('hidden');
    $('.nav-tabs li:eq(1) a').tab('show')
}
if (availablePerk) {
    document.getElementById('perks-tab-link').classList.remove('hidden');
    document.getElementById('perks-tab-content').classList.remove('hidden');

    document.getElementById('perks-tab-content').classList.remove('perks-alerts-yellow');
    document.getElementById('perks-tab-content').classList.remove('perks-alerts-red');
    document.getElementById('perks-tab-content').classList.remove('perks-alerts-green');
    
    if (modul === 0 && currentLevel < 25) {
        document.getElementById('perks-tab-content').classList.add('perks-alerts-green');
    }
    if (modul === 1 && currentLevel < 25) {
        document.getElementById('perks-tab-content').classList.add('perks-alerts-yellow');
    }
    if (modul >= 2 && currentLevel < 25) {
        document.getElementById('perks-tab-content').classList.add('perks-alerts-red');
    }
    if (currentLevel === 25) {
        document.getElementById('perks-tab-content').classList.add('perks-alerts-green');
    };
    if (currentLevel === 26 || currentLevel === 27) {
        document.getElementById('perks-tab-content').classList.add('perks-alerts-yellow');
    }
    if (currentLevel === 28 || currentLevel === 29) {
        document.getElementById('perks-tab-content').classList.add('perks-alerts-red');
    }
}

}

function addPerk (value) {
    let perkAdd = 'perk' + value;
    let perkRemove = value + 'perk';
    let i = Number(value);
    
    if (value.length < 3) {
        document.getElementById(`${perkAdd}`).classList.remove('hidden');
        document.getElementById(`${perkRemove}`).parentElement.classList.add('hidden-2');
        document.getElementById('perks-tab-content').classList.add('hidden');
        availablePerk = 0;
        nextPerkAtLevel = Math.floor(currentLevel/requiredLevel[0]);
        nextPerkAtLevel = requiredLevel[nextPerkAtLevel];
        arrChar[4][i].tagged = true;
        stepArrChar = JSON.parse(JSON.stringify(arrChar));
    } else if (value.length === 3) {
        document.getElementById(`${perkAdd}`).classList.remove('hidden');
        document.getElementById(`${perkRemove}`).parentElement.classList.add('hidden-2');
        arrChar[5][i].tagged = true;
        stepArrChar = JSON.parse(JSON.stringify(arrChar));
    }
    
    availablePerks();
    availableSupportPerks();
}

function availableSupportPerks () {
    arrChar[5].forEach( supportPerk => {
        supportPerk.func();
        const supportPerkID = (supportPerk.id).slice(-3) + 'perk';
        if(supportPerk.available) {
            document.getElementById(`${supportPerkID}`).parentElement.classList.remove('hidden');
        } else {
            document.getElementById(`${supportPerkID}`).parentElement.classList.add('hidden');
        }
        });
    // checks for more than 1 available support perks and toggles the Take All button
    const activePerks = document.getElementsByClassName('support-perk');
    let activePerkCounter = 0;
    for (const activePerk of activePerks) {
        if (activePerk.className === 'support-perk') {
            activePerkCounter++;
            if (activePerkCounter > 1) {
                document.getElementById('take-all-btn').classList.remove('hidden');

            } else {
                document.getElementById('take-all-btn').classList.add('hidden');

            }
        }
    }
    if (activePerkCounter === 0) {
        document.getElementById('support-perks-tab-link').classList.add('hidden');
        document.getElementById('support-perks-tab').classList.add('hidden');
        document.getElementById('support-perks-content').classList.add('hidden');
    } else {
        document.getElementById('support-perks-tab-link').classList.remove('hidden');
        document.getElementById('support-perks-tab').classList.remove('hidden');
        document.getElementById('support-perks-content').classList.remove('hidden');
    }

}

function takeAllSupportPerks () {
    const supportPerks = document.getElementsByClassName('support-perk');
            for (const supportPerk of supportPerks) {
                if (supportPerk.className === 'support-perk') {
                    addPerk(`${supportPerk.childNodes[0].value}`);
                    console.log('Perk removed: ' + supportPerk.childNodes[0].innerText);
                }                
            }
            document.getElementById('take-all-btn').classList.add('hidden');
            stepSkillsArrChar = JSON.parse(JSON.stringify(skillsArrChar));
}

function updateProgressBars () {
    const bars = document.querySelectorAll('[name="progress-bar-skills"]');
    let i = 0;
    for (const bar of bars) {
        bar.style = `width: ${skillsArrChar[i].value/(skillsArrChar[i].max/100)}%`;
        i++;
    }
}

function availableProfessions () {
    i = 0;
    let profCounter = 0;
    arrChar[9].forEach (prof => {
        arrChar[9][i].func();

        if (prof.available && !prof.tagged) {
            document.getElementById(`${(prof.id)}-${i}`).classList.remove('hidden');
            profCounter++;
        } else if (prof.available && prof.tagged) {
            document.getElementById(`${(prof.id)}-${i}`).classList.add('hidden');
        } else {
            document.getElementById(`${(prof.id)}-${i}`).classList.add('hidden');
        }
        i++;
    });

    if (!profCounter) {
        document.getElementById('available-professions-header').classList.add('hidden');
        document.getElementById('prof-panel').classList.add('hidden');

    }
    if (profCounter) {
        document.getElementById('available-professions-header').classList.remove('hidden');
        document.getElementById('prof-panel').classList.remove('hidden');

    }
}

function takeProfession (value) {

    let profElement = value.slice(0, -3);
    let profIndex = Number(value.slice(-2));
    console.log(profIndex);
    let i = 0;
    arrChar[9][profIndex].tagged = true;
    arrChar[9][profIndex].available = false;
    console.log(profIndex%2);
    if (profIndex % 2 === 0) {
        document.getElementById(`${profElement}`).classList.remove('hidden');
        document.getElementById(`${profElement}`).innerText = arrChar[9][profIndex].name + '(1)';
        document.getElementById(`${profElement}-${profIndex}`).classList.add('hidden-2');
    } else {
        document.getElementById(`${profElement}`).classList.remove('hidden');
        document.getElementById(`${profElement}`).innerText = arrChar[9][profIndex].name + '(2)';
        document.getElementById(`${profElement}-${profIndex}`).classList.add('hidden-2');
        
    }
    
    console.log(`${profElement}-${profIndex}`);
    availableProfessions();
}

function takeDrug (id) {
    let drugIndex = Number(id.slice(-1));
    let value = document.getElementById(id).value;
    if (value === 'OFF') {
        document.getElementById(id).value = 'ON';
        document.getElementById(id).firstChild.classList.replace('image-drug','image-drug-selected');
        document.getElementById(id).firstChild.title = `Remove ${arrChar[10][drugIndex].name}`;
        arrChar[10][drugIndex].tagged = true;
    } else {
        document.getElementById(id).value = 'OFF';
        document.getElementById(id).firstChild.classList.replace('image-drug-selected', 'image-drug');
        document.getElementById(id).firstChild.title = `Take ${arrChar[10][drugIndex].name}`;
        arrChar[10][drugIndex].tagged = false;
    }
    let i = 0;
    arrChar[1].forEach(stat => {
        if (i > 6) return;
        const drugId = `drugs-${(stat.id).toUpperCase()}`;
        const specialValue = arrChar[1][i].onDrugs();
        if (specialValue > 10) {
            document.getElementById(drugId).value = 10;
        } else if (specialValue < 1) {
            document.getElementById(drugId).value = 1;
        } else {
            document.getElementById(drugId).value = specialValue;
        }
        i++;
    });    
}

function availableBoosts () {
    let counter = 0;
    arrChar[11].forEach(boost => {
        if (!boost.id) return;
        boost.available = boost.func();
        if (boost.available) {
            document.getElementById(`${boost.id}`).classList.remove('hidden');
            counter++;
        } else {
            document.getElementById(`${boost.id}`).classList.add('hidden');
        }
    });
    if (!counter) document.getElementById('dropdown-div').classList.add('hidden');
    if (counter) document.getElementById('dropdown-div').classList.remove('hidden');
}

function addBoost (id) {
    const skillId = Number(id.slice(3,5));
    const rSkill = `r-${id.slice(0,5).toUpperCase()}`;
    const skillValue = id.slice(0,5).toUpperCase();
    arrChar[2][skillId].value += 10;
    stepSkillsArrChar = JSON.parse(JSON.stringify(skillsArrChar));
    addPerk(003);
    document.getElementById('perk003').TEXT_NODE = `${arrChar[11][skillId].name}`;
    document.getElementById(rSkill).checked = true;
    document.getElementById(skillValue).value = `${arrChar[2][skillId].value}%`;
    displaySkill();
    availableSupportPerks();
    updateProgressBars();
    document.getElementById('dropdown-div').classList.add('hidden-2');
    console.log(skillId);
}

function availableImplants() {
    let counter = 0;
    arrChar[8].forEach(implant => {
        implant.func();
        if (implant.available) {
            const implantId = (implant.id).slice(0,-2);
            console.log(implantId);
            document.getElementById(implantId).classList.remove('hidden');
            counter++;
        }
    });
    if (!counter) {
        document.getElementById('implants-panel').classList.add('hidden');
        document.getElementById('available-implants-header').classList.add('hidden');
    } else {
        document.getElementById('implants-panel').classList.remove('hidden');
        document.getElementById('available-implants-header').classList.remove('hidden');
    }
    
}

function takeImplant(value) {
    document.getElementById(value).classList.add('hidden-2');
    const implantId = `${value}ed`;
    const specialStatId = `${(value.slice(0,5).toUpperCase())}`;
    const index = Number(value.slice(3,5));
    if (index < 7) {
        arrChar[1][index].value += 1;
        document.getElementById(specialStatId).classList.replace('special-values-box','special-values-box-implanted');
        document.getElementById(implantId).classList.remove('hidden');
    } else {
        document.getElementById(implantId).classList.remove('hidden');
    }
    availableImplants();
    let i = 0;
    arrChar[1].forEach(stat => {
        if (i > 6) return;
        document.getElementById(`${(stat.id).toUpperCase()}`).value = stat.value;
        i++;
    });

}